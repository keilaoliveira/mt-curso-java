package exercicios.exercicioEnum;

public class Principal {
	
	public static void main(String[] args) {
		
		System.out.println(Cores.ROXO.getTexto());
		System.out.println(Cores.VERMELHO.getTexto());
		System.out.println(Cores.AZUL.getTexto());
		System.out.println(Cores.CINZA.getTexto());
		System.out.println(Cores.ROSA.getTexto());
	}

}
