package exercicios.exercicioEnum;

public enum Cores {

	ROXO("Roxo"), VERMELHO("Vermelho"), ROSA("Rosa"), CINZA("Cinza"), AZUL("Azul");

	private String texto;

	Cores(String texto) {
		this.setTexto(texto);
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}
