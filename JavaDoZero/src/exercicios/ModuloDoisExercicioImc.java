package exercicios;

import java.util.Scanner;

public class ModuloDoisExercicioImc {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int opcao;
        try {
            do {
                System.out.println("|    MENU             !");
                System.out.println("| Opções:             !");
                System.out.println("|    1. Calcular IMC  !");
                System.out.println("|    0. Sair          !");
                System.out.print("\n" + "Selecione uma opção: ");

                opcao = scanner.nextInt();

                if (opcao == 1) {
                    double peso, altura, imc;

                    System.out.print("Digite seu peso(Em Kg - Ex. 80,5): ");
                    peso = scanner.nextDouble();

                    System.out.print("Digite sua altura(Em Metros - Ex. 1,70): ");
                    altura = scanner.nextDouble();

                    imc = peso / Math.pow(altura, 2);

                    if (imc <= 18.5) {
                        System.out
                                .println("\n" + "Seu peso é : " + peso + "\n" + "Sua altura é: " + altura + "\n" + "E você está abaixo do peso ideal." + "\n");
                    } else if (imc > 18.5 & imc <= 24.9) {
                        System.out.println("\n" + "Seu peso é : " + peso + "\n" + "Sua altura é: " + altura + "\n"
                                + "E você está no peso ideal." + "\n");
                    } else if (imc > 24.9 & imc <= 30) {
                        System.out.println("\n" + "Seu peso é : " + peso + "\n" + "Sua altura é: " + altura + "\n"
                                + "E você está no peso do nível de pré obesidade." + "\n");
                    } else if (imc > 30) {
                        System.out.println("\n" + "Seu peso é : " + peso + "\n" + "Sua altura é: " + altura + "\n"
                                + "E você está no peso do nível de obesidade." + "\n");
                    }
                } else if (opcao == 0) {
                    System.out.println("Programa Finalizado." + "\n");
                } else {
                    System.out.println("Opção inválida. Selecione '1' para Calcular IMC ou '0' para sair." + "\n");
                }

            } while (opcao != 0);
        } catch (Exception e) {
            System.out
                    .println("\n" + "Não foi possível completar sua solicitação!" + "\n");
        }
        scanner.close();
    }
}
