package exercicios.carro;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {

		try {
			Scanner scanner = new Scanner(System.in);

			System.out.println("|| Digite os dados do veículo ||" + "\n");
			System.out.print("Qual a quantidade de Pneus? ");
			Integer quantidadePneus = scanner.nextInt();

			System.out.print("Qual o chassi? ");
			String chassi = scanner.next();

			System.out.print("Qual o ano de Fabricação? ");
			Integer anoFabricacao = scanner.nextInt();

			System.out.print("Qual o Combustível (Flex/Gasolina/Álcool)? ");
			String combustivel = scanner.next();

			System.out.print("Qual a cor?  ");
			String cor = scanner.next();

			System.out.print("Qual o tipo de som (Multimidia/Padrão)? ");
			String som = scanner.next();

			System.out.print("Possui ar-condicionado? (S/N) ");
			String arcondicionado = scanner.next();

			Carro carro = new Carro(quantidadePneus, chassi, anoFabricacao, combustivel);
			carro.setArCondicionado(arcondicionado);
			carro.setSom(som);
			carro.setCor(cor);

			carro.ImprimeValores();

			scanner.close();

		} catch (Exception e) {
			System.out.println("Não foi possível registrar o veículo! Verifique os dados informados!");
		}

	}

}
