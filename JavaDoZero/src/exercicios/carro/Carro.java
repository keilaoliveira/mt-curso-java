package exercicios.carro;

public class Carro {

	private Integer quantidadePneus;
	public Integer quantidadeCalotas;
	public Integer quantidadeParafusosPneu;
	private String cor;
	private String arCondicionado;
	private String chassi;
	private Integer anoFabricacao;
	private String combustivel;
	private String som;

	public Carro(Integer quantidadePneus, String chassi, Integer anoFabricacao, String combustivel) {
		this.quantidadePneus = quantidadePneus;
		this.chassi = chassi;
		this.anoFabricacao = anoFabricacao;
		this.combustivel = combustivel;

	}

	public void ImprimeValores() {
		System.out.println("\n" + "--- O carro será entregue com as seguintes especificações ---");
		System.out.println("Cor: " + getCor());
		System.out.println("Chassi: " + getChassi());
		System.out.println("Ano de Fabricação: " + getAnoFabricacao());
		System.out.println("Quantidade de Pneus: " + getQuantidadePneus());
		System.out.println("Quantidade de Calotas: " + getQuantidadeCalotas());
		System.out.println("Quantidade de Parafusos: " + getquantidadeParafusosPneu());
		System.out.println("Ar-Condicionado: " + getArCondicionado());
		System.out.println("Som: " + getSom());
		
		System.out.println("\n" + "Aproveite o Veículo!!!");
		
	}

	public Integer getQuantidadePneus() {
		setQuantidadeCalotas(quantidadePneus);
		setquantidadeParafusosPneu(quantidadePneus * 4);
		return quantidadePneus + 1;
	}

	public void setQuantidadePneus(Integer quantidadePneus) {
		this.quantidadePneus = quantidadePneus;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getArCondicionado() {
		return arCondicionado;
	}

	public void setArCondicionado(String arCondicionado) {
		this.arCondicionado = arCondicionado;
	}
	

	public Integer getQuantidadeCalotas() {
		return quantidadeCalotas;
	}

	public void setQuantidadeCalotas(Integer quantidadeCalotas) {
		this.quantidadeCalotas = quantidadeCalotas;
	}

	public Integer getquantidadeParafusosPneu() {
		return quantidadeParafusosPneu;
	}	

	public void setquantidadeParafusosPneu(Integer quantidadeParafusosPneu) {
		this.quantidadeParafusosPneu = quantidadeParafusosPneu;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public String getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}

	public String getSom() {
		return som;
	}

	public void setSom(String som) {
		this.som = som;
	}
}
