package exercicios;

public class ModuloCincoExercicioWrapper {

	public static void main(String[] args) {

		Byte valorByte = 126;
		Short valorShort = 30796;
		Integer valorInt = 4567;
		Character valorChar = 'K';
		String valorString = String.valueOf(valorChar);
		Long valorLong = 34567098L;
		Double valorDouble = 18D;
		Float valorFloat = 1.58F;
		Boolean valorBoolean = true;
		
		System.out.println("Double: " + valorDouble);
		System.out.println("Byte: " + valorByte);
		System.out.println("Short: " + valorShort);
		System.out.println("Integer: " + valorInt);
		System.out.println("Char: " + valorChar);
		System.out.println("String: " + valorString);
		System.out.println("Long: " + valorLong);
		System.out.println("Float: " + valorFloat);
		System.out.println("Boolean: " + valorBoolean);

	}

}
