package exercicios.Modulo8ExercicioInterfaces;

public abstract class Conta {
	private int numero;
	private int agencia;
	protected double saldo = 0;

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public abstract double getSaldo();

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Conta(int numero, int agencia, double saldo) {
		this.numero = numero;
		this.agencia = agencia;
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return "Conta:" + "numero=" + numero + ", agencia=" + agencia +  ", saldo=" + saldo + '}';
	}

}
