package exercicios.Modulo8ExercicioInterfaces;

public class ContaPoupanca extends Conta {
	private int diaAniversario;
	private double taxaDeJuros;

	public ContaPoupanca(int numero, int agencia, double saldo, int diaAniversario, double taxaDeJuros) {
		super(numero, agencia, saldo);
		this.diaAniversario = diaAniversario;
		this.taxaDeJuros = taxaDeJuros;

	}

	public double getSaldo() {
		return this.saldo + this.taxaDeJuros * this.saldo;

	}
	
	public double getTaxaDeJuros() {
		return taxaDeJuros;
	}

	public void setTaxaDeJuros(Double taxaDeJuros) {
		this.taxaDeJuros = taxaDeJuros;
	}
	
	public void depositar(Double valor) {
		setSaldo(saldo + valor);
		System.out.println("Saldo da Poupança atualizado: " + getSaldo());
	}

	public void sacar(Double valor) {

		if (valor <= getSaldo()) {
			setSaldo(getSaldo() - valor);
			System.out.println("Saldo da Conta Poupança atualizado: " + getSaldo());
		} else {
			System.out.println("Saque não permitido. O saldo é de: " + getSaldo()
					+ "\n A quantia desejada do saque é de: " + valor);
		}
	}
	
	public void taxaDeJurosConta(int dia) {
		if (dia >= diaAniversario) {
			setSaldo(saldo + (taxaDeJuros * saldo/100));
		}
	}	

}
