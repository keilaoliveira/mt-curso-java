package exercicios.Modulo8ExercicioInterfaces;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		Operacoes operacoes = new Operacoes();

		int option = 0;
		do {

			System.out.println();
			System.out.println("------------------------Bem vindo ao banco------------------------");
			System.out.println("------Preencha os dados a seguir para realizar sua transação------");
			System.out.println("------------------------------------------------------------------");
			System.out.println("| ---------  Criar ---------   |");
			System.out.println("| 1.Conta Corrente             |");
			System.out.println("| 2.Conta Poupança             |");
			System.out.println("| 3.Conta Salário              |");
			System.out.println("");
			System.out.println("| ---------  Sacar ---------   |");
			System.out.println("| 4.Conta Corrente             |");
			System.out.println("| 5.Conta Poupança             |");
			System.out.println("| 6.Conta Salário              |");
			System.out.println("");
			System.out.println("| --------  Depositar -------- |");
			System.out.println("| 7.Conta Corrente             |");
			System.out.println("| 8.Conta Poupança             |");
			System.out.println("| 9.Conta Salário              |");
			System.out.println("");			
			System.out.println("| 10.Transferência             |");
			System.out.println("| 11.Valor total das contas    |");
			System.out.println("| 12.Listar contas cadastradas |");
			System.out.println("| 0. Sair                      |");
			System.out.println("------------------------------------------------------------------");

			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(System.in);
			System.out.print("Qual operação deseja realizar? ");
			option = scanner.nextInt();

			try {
				switch (option) {
				case 1: 
					//Criar Conta Corrente
					System.out.println("Você irá criar uma Conta Corrente!");					
					
					Scanner entrada1 = new Scanner(System.in);
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCC = entrada1.nextLine();

					operacoes.criarConta(nomeTitularCC);
					operacoes.criarContaCorrente(nomeTitularCC);
					break;
				case 2: 
					//Criar Conta Poupança
					System.out.println("Você irá criar uma Conta Poupança!");					
					
					Scanner entrada2 = new Scanner(System.in);
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCP = entrada2.nextLine();

					operacoes.criarConta(nomeTitularCP);
					operacoes.criarContaPoupanca(nomeTitularCP);
					break;
				case 3: 
					//Criar Conta Salário
					System.out.println("Você irá criar uma Conta Salário!");					
					
					Scanner entrada3 = new Scanner(System.in);
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCS = entrada3.nextLine();

					operacoes.criarConta(nomeTitularCS);
					operacoes.criarContaSalario(nomeTitularCS);
					break;					
				case 4:
					//Sacar Conta Corrente
					System.out.println("Você irá realizar um saque na Conta Corrente!");					
					
					Scanner entrada4 = new Scanner(System.in);					
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCC2 = entrada4.nextLine();	
					
					operacoes.sacarContaCorrente(nomeTitularCC2);
					break;
				case 5:
					//Sacar Conta Poupança
					System.out.println("Você irá realizar um saque na Conta Poupança!");					
					
					Scanner entrada5 = new Scanner(System.in);					
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCP2 = entrada5.nextLine();	
					
					operacoes.sacarContaPoupanca(nomeTitularCP2);
					break;	
				case 6:
					//Sacar Conta Salário
					System.out.println("Você irá realizar um saque na Conta Salário!");					
					
					Scanner entrada6 = new Scanner(System.in);					
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCS2 = entrada6.nextLine();	
					
					operacoes.sacarContaSalario(nomeTitularCS2);
					break;					
				case 7:
					//Depositar Conta Corrente					
					System.out.print("Você irá realizar um depósito na Conta Corrente!");					
					
					Scanner entrada7 = new Scanner(System.in);	
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCC3 = entrada7.nextLine();
					
					operacoes.depositarContaCorrente(nomeTitularCC3);
					break;
				case 8:
					//Depositar Conta Poupança		
					System.out.println("Você irá realizar um depósito na Conta Poupança!");					
					
					Scanner entrada8 = new Scanner(System.in);
					System.out.print("Você irá realizar um depósito na Conta Poupança!");					
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCP3 = entrada8.nextLine();
					
					operacoes.depositarContaCorrente(nomeTitularCP3);
					break;	
				case 9:
					//Depositar Conta Salário	
					System.out.println("Você irá realizar um depósito na Conta Salário!");					
					
					Scanner entrada9 = new Scanner(System.in);
					System.out.print("Você irá realizar um depósito na Conta Salário!");					
					System.out.print("Digite o nome do titular da conta: ");
					String nomeTitularCS3 = entrada9.nextLine();
					
					operacoes.depositarContaCorrente(nomeTitularCS3);
					break;					
				case 10:
					//Transferência
					System.out.println("Você irá realizar uma tranferência entre contas!");					
					
					Scanner entrada10 = new Scanner(System.in);					
					System.out.print("Digite o nome do titular da conta que irá fazer a transferência: ");
					String nomeTitularOrigem = entrada10.nextLine();
					
					System.out.print("Digite o nome do titular da conta que irá receber a transferência: ");
					String nomeTitularDestino = entrada10.nextLine();
					
					System.out.println("Digite de qual conta será feita a transferência - Origem (Digite apenas o número da opção (1,2 ou 3): ");
					System.out.print("1 - Conta Corrente | 2 - Conta Poupança | 3 - Conta Salário : ");					
					int tipoContaOrigem = entrada10.nextInt();
					
					System.out.println("Digite para qual conta será feita a transferência - Destino (Digite apenas o número da opção (1,2 ou 3): ");
					System.out.print("1 - Conta Corrente | 2 - Conta Poupança | 3 - Conta Salário : ");					
					int tipoContaDestino = entrada10.nextInt();
					
					System.out.print("Qual o valor da transferência ? ");
					Double valor = entrada10.nextDouble();
					
					operacoes.transferir(tipoContaOrigem, tipoContaDestino, valor, nomeTitularOrigem, nomeTitularDestino);
					break;
				case 11:
					//Saldo Total da conta de um cliente específico
					operacoes.Saldo();
					break;
				case 12:
					//Lista de todas as contas
					operacoes.listarContas();
					break;
				case 0:
					System.out.println("A operação foi finalizada. Volte Sempre!");
					break;
				default:
					System.out.println("Opção inválida - Digite uma opção do 0 ao 12! ");
					break;
				}

			} catch (Exception e) {
				System.out.println("O valor digitado é inválido. Revise o valor digitado e tente novamente.");
			}
		} while (option != 0);

	}
}
