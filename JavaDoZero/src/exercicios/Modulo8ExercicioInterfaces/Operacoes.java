package exercicios.Modulo8ExercicioInterfaces;

import java.util.ArrayList;
import java.util.Scanner;

public class Operacoes {

	protected ArrayList<Banco> contas = new ArrayList<>();

	public void criarConta(String nome) {
		Banco banco = new Banco();
		banco.setNomeCliente(nome);
		contas.add(banco);
	}

	public void criarContaPoupanca(String nome) {
		int contador = 0;

		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome) && contador < 1) {
				
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.print("Digite o numero da conta: ");
				int numeroDeContaCP = entrada.nextInt();

				System.out.print("Digite o numero da agência: ");
				int numeroAgenciaCP = entrada.nextInt();

				System.out.print("Digite o saldo inicial da Conta: ");
				Double saldoInicialCP = entrada.nextDouble();

				System.out.print("Qual o dia? (Digite o dia para cálculo dos rendimentos): ");
				int diaCP = entrada.nextInt();

				conta.criarContaPoupanca(numeroDeContaCP, numeroAgenciaCP, saldoInicialCP, diaCP, 0.02);
				System.out.println("Conta Poupança Cadastrada com Sucesso!!");
				System.out.println("");
				contador = contador + 1;
			}
		}
	}

	public void criarContaCorrente(String nome) {
		int contador = 0;

		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome)  && contador < 1) {
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.print("Digite o numero da conta:");
				int numeroDeContaCC = entrada.nextInt();

				System.out.print("Digite o numero da agência:");
				int numeroAgenciaCC = entrada.nextInt();

				System.out.print("Digite o saldo inicial da Conta: ");
				Double saldoInicialCC = entrada.nextDouble();

				System.out.print("Digite o valor do seu cheque especial: ");
				Double chequeCC = entrada.nextDouble();

				conta.criarContaCorrente(numeroDeContaCC, numeroAgenciaCC, saldoInicialCC, chequeCC);
				System.out.println("Conta Corrente Cadastrada com Sucesso!!");
				System.out.println("");
				contador = contador + 1;
			}
		}
	}

	public void criarContaSalario(String nome) {
		int contador = 0;

		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome) && contador < 1) {
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.print("Digite o numero da conta:");
				int numeroDeContaCS = entrada.nextInt();

				System.out.print("Digite o numero da agência:");
				int numeroAgenciaCS = entrada.nextInt();

				System.out.print("Digite o saldo inicial da Conta: ");
				Double saldoInicialCS = entrada.nextDouble();

				System.out.println("Qual a quantidade de saques diários? ");
				int saquesDiariosCS = entrada.nextInt();

				conta.criarContaSalario(numeroDeContaCS, numeroAgenciaCS, saldoInicialCS, saquesDiariosCS);
				System.out.println("Conta Salário Cadastrada com Sucesso!!");
				System.out.println("");
				contador = contador + 1;
			}
		}
	}

	public void sacarContaPoupanca(String nome) {
		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome)) {
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.println("Conta Titular: " + conta.getNomeCliente() + " - Saldo Disponível (Conta Poupança): "
						+ conta.ContaPoupanca.getSaldo());
				System.out.print("Qual o valor do saque? ");
				Double valor = entrada.nextDouble();

				conta.sacarContaPoupanca(valor);
			}
		}
	}

	public void sacarContaCorrente(String nome) {
		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome)) {
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.println("Conta Titular: " + conta.getNomeCliente() + " - Saldo Disponível (Conta Corrente): "
						+ conta.ContaCorrente.getSaldo());
				System.out.print("Qual o valor do saque? ");
				Double valor = entrada.nextDouble();

				conta.sacarContaCorrente(valor);
			}
		}
	}

	public void sacarContaSalario(String nome) {
		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome)) {
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.println("Conta Titular: " + conta.getNomeCliente() + " - Saldo Disponível (Conta Salário): "
						+ conta.ContaSalario.getSaldo());
				System.out.print("Qual o valor do saque? ");
				Double valor = entrada.nextDouble();

				conta.sacarContaSalario(valor);
			}
		}
	}

	public void depositarContaPoupanca(String nome) {
		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome)) {
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.println("Conta Titular: " + conta.getNomeCliente() + " - Saldo Disponível (Conta Salário): "
						+ conta.ContaPoupanca.getSaldo());
				System.out.print("Qual o valor do depósito? ");
				Double valor = entrada.nextDouble();

				conta.depositarContaPoupanca(valor);
			}
		}
	}

	public void depositarContaSalario(String nome) {
		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome)) {
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.println("Conta Titular: " + conta.getNomeCliente() + " - Saldo Disponível (Conta Salário): "
						+ conta.ContaSalario.getSaldo());
				System.out.print("Qual o valor do depósito? ");
				Double valor = entrada.nextDouble();

				conta.depositarContaSalario(valor);
			}
		}
	}

	public void depositarContaCorrente(String nome) {
		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nome)) {
				@SuppressWarnings("resource")
				Scanner entrada = new Scanner(System.in);

				System.out.println("Conta Titular: " + conta.getNomeCliente() + " - Saldo Disponível (Conta Salário): "
						+ conta.ContaCorrente.getSaldo());
				System.out.print("Qual o valor do depósito? ");
				Double valor = entrada.nextDouble();

				conta.depositarContaCorrente(valor);
			}
		}
	}

	public void Saldo() {
		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);

		System.out.print("Digite o nome do titular da conta: ");
		String nomeTitular = entrada.nextLine();

		for (Banco conta : this.contas) {
			if (conta.getNomeCliente().equals(nomeTitular)) {
				System.out.println(conta.getNomeCliente());
				System.out.println(conta.Saldo());
			}
		}
	}

	public void listarContas() {
		System.out.println("--------------- Clientes Cadastrados ---------------");
		for (Banco conta : this.contas) {
			System.out.println("Nome: " + conta.getNomeCliente() + " |  Saldo da conta: " + conta.Saldo()
					+ " | Numero da Conta: " + conta.numeroDeConta());
		}
	}

	public void transferir(int tipoDeConta, int tipoDeContaDestino, double valor, String nomeTitularOrigem, String nomeTitularDestino) {
		for (Banco conta : contas) {
			if (conta.getNomeCliente().equals(nomeTitularOrigem)) {
				conta.transferir(valor, tipoDeConta, "saque");
				conta.setNomeCliente(nomeTitularDestino);
			}

			if (conta.getNomeCliente().equals(nomeTitularDestino)) {
				conta.transferir(valor, tipoDeContaDestino, "deposito");
			}
		}
	}
}
