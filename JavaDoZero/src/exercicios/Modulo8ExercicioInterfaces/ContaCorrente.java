package exercicios.Modulo8ExercicioInterfaces;

public class ContaCorrente extends Conta implements Tributavel {
	private double chequeEspecial;

	public ContaCorrente(int numero, int agencia, double saldo, double chequeEspecial) {
		super(numero, agencia, saldo);
		this.chequeEspecial = chequeEspecial;
	}

	@Override
	public String toString() {
		return "ContaCorrente:  " + "chequeEspecial = " + chequeEspecial + '}';
	}

	@Override
	public double taxaJuros() {
		return this.chequeEspecial * 0.05;

	}

	public double getSaldo() {
		return this.chequeEspecial + saldo;
	}
	
	public void sacar(Double valor) {

		System.out.println("Saque solicitado no valor de: " + valor);
		if (valor <= (getSaldo())) {
			setSaldo(saldo - valor);

			System.out.println("Saldo atualizado: " + getSaldo() + "\n");

		} else {
			System.out.println("Seu saldo é de: " + getSaldo());
			System.out.println("Infelizmente você não tem saldo para sacar essa quantia.\n");
		}
	}

	public void depositar(Double valor) {
		System.out.println("O depósito será no valor de: " + valor);
		setSaldo(saldo + valor);

		System.out.println("Saldo atualizado: " + getSaldo());
	}



}
