package exercicios.Modulo8ExercicioInterfaces;

public class ContaSalario extends Conta implements Tributavel {
	private int limiteSaques;

	public ContaSalario(int numero, int agencia, double saldo, int limiteSaques) {
		super(numero, agencia, saldo);
		this.limiteSaques = limiteSaques;
	}

	public double getSaldo() {
		return this.saldo;

	}

	@Override
	public double taxaJuros() {
		return this.getSaldo() * 0.02;
	}

	public void sacar(Double valor) {

		if (limiteSaques > 0) {
			setSaldo(getSaldo() - valor);
			limiteSaques--;
			System.out.println(
					"Saldo atualizado: R$ " + getSaldo() + ". Você ainda pode sacar " + limiteSaques + " vez hoje!");
		} else {
			System.out
					.println("Você atingiu seu limite de saques diário. Novo saque disponível no próximo dia útil. \n");
		}

	}

	public void depositar(Double valor) {
		setSaldo(getSaldo() + valor);
		System.out.println("Saldo atualizado: R$ " + getSaldo());
	}

}
