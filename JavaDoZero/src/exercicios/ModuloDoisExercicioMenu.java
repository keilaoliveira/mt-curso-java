package exercicios;

import java.util.Scanner;

public class ModuloDoisExercicioMenu {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int opcao;
        try {
            do {
                System.out.println("|    MENU             !");
                System.out.println("| Opções:             !");
                System.out.println("|    1. Opção 1       !");
                System.out.println("|    2. Opção 2       !");
                System.out.println("|    3. Opção 3 (Sair)!");
                System.out.print("\n" + "Selecione uma opção: ");

                opcao = scanner.nextInt();

                executar(opcao);

            } while (opcao != 3);
        } catch (Exception e) {
            System.out.println("\n" + "Não foi possível completar sua solicitação! São aceitos somente números!" + "\n");
        }
        scanner.close();
    }

    public static void executar(int opcao) {
        switch (opcao) {
        case 1:
            System.out.println("\n" + "Opção 1 Selecionada" + "\n");
            break;
        case 2:
            System.out.println("\n" + "Opção 2 Selecionada" + "\n");
            break;
        case 3:
            System.out.println("\n" + "O Programa foi finalizado!" + "\n");
            break;
        default:
            System.out.println("\n" + "Seleção Inválida!" + "\n");
            break;
        }
    }
}
