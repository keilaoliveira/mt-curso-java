package exercicios;

public class ModuloSeisExercicioChamada {

	public static void main(String[] args) {

		String alunos[] = { "Paulo", "Pedro", "Thais", "Karla" };
		String datas[] = { "10/05/2022", "11/05/2022", "13/05/2022", "14/05/2022" };
		String presente[] = { "sim", "não", "sim", "não" };
		String[][] chamada = new String[alunos.length][datas.length];

		System.out.println("------------------------------Diário de Classe------------------------------");
		System.out.println("Lista de Presença: \n");

		int linha, coluna;
		int i;

		for (i = 0; i < 2; i++) {
			System.out.print("\t");
		} // é para dar o espaço da primeira coluna da data

		for (i = 0; i < datas.length; i++) {
			System.out.print(datas[i] + "\t");
		} // colunas de datas na horizontal

		for (i = 0; i < alunos.length; i++) {
			for (linha = 0; linha < chamada.length; linha++) {
				System.out.println("\t\t");// para deixar a tabela formatada
				System.out.print(alunos[i] + "\t\t");
				i = i + 1;

				for (coluna = 0; coluna < presente.length; coluna++) {
					System.out.print(presente[coluna] + "\t\t");
				}
			}
		}

	}

}
