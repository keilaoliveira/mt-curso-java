package exercicios.ModuloSeteExerciciosConta;

public class ContaCorrente extends Conta {

	public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
		super(numero, agencia, banco, saldo);
		this.chequeEspecial = chequeEspecial;
	}
	
	private double chequeEspecial;

	public double getChequeEspecial() {
		return chequeEspecial;
	}

	public void setChequeEspecial(double chequeEspecial) {
		this.chequeEspecial = chequeEspecial;
	}

	@Override
	public String toString() {
		return "ContaCorrente [chequeEspecial=" + chequeEspecial + "]";
	}

	public double getSaldo() {
		return this.chequeEspecial + saldo;
	}
	
	@Override
	public double Sacar(double quantia) {
		System.out.println("Saque solicitado no valor de: " + quantia);
		if (quantia <= (getSaldo())){
			setSaldo(saldo - quantia);
				
		System.out.println("Saldo atualizado: " + getSaldo() + "\n");
		
		}else {
			System.out.println("Seu saldo é de: " + getSaldo());
			System.out.println("Infelizmente você não tem saldo para sacar essa quantia.\n");
		}
		return 0.0;
	}
	
	@Override
	public double Depositar(double quantia) {
		System.out.println("O depósito será no valor de: " + quantia);
		setSaldo(saldo + quantia);
		
		System.out.println("Saldo atualizado: " + getSaldo());
		return quantia;
	}

}
