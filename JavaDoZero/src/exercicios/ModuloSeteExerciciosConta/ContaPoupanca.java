package exercicios.ModuloSeteExerciciosConta;

public class ContaPoupanca extends Conta{
		
	private int diaAniversario;
	private double taxaDeJuros;
	public ContaPoupanca(int numero, int agencia, String banco, double saldo, int diaAniversario, double taxaDeJuros) {
		super(numero, agencia, banco, saldo);
		this.diaAniversario = diaAniversario;
		this.taxaDeJuros = taxaDeJuros;
	}
	
	public double getSaldo() {
		return this.saldo;
	}
	
	public int getDiaAniversario() {
		return diaAniversario;
	}

	public void setDiaAniversario(int diaAniversario) {
		this.diaAniversario = diaAniversario;
	}

	public double getTaxaDeJuros() {
		return taxaDeJuros;
	}

	public void setTaxaDeJuros(double taxaDeJuros) {
		this.taxaDeJuros = taxaDeJuros;
	}
	
	@Override
	public double Sacar(double quantia) {
		if (quantia <= getSaldo()) {
			setSaldo(getSaldo() - quantia);
			System.out.println("Saldo da Conta Poupança atualizado: " + getSaldo());
			return quantia;
		}else {
			System.out.println("Saque não permitido. O saldo é de: " + getSaldo() + "\n A quantia desejada do saque é de: " + quantia);
		}
		return 0.0;
	}

	public void taxaDeJurosConta(int dia) {
		if (dia >= diaAniversario) {
			setSaldo(saldo + (taxaDeJuros * saldo/100));
		}
	}
	
	@Override
	public double Depositar(double quantia) {
		setSaldo(saldo + quantia);
		System.out.println("Saldo da Poupança atualizado: " + getSaldo());		
		return quantia;
	}

}
