package exercicios.ModuloSeteExerciciosConta;

import java.util.Scanner;

public class TesteConta {

	public static void main(String[] args) throws Exception {

		int option;
		do {
			System.out.println();
			System.out.println("------------------------Bem vindo ao banco------------------------");
			System.out.println("------Preencha os dados a seguir para realizar sua transação------");
			System.out.println("------------------------------------------------------------------");
			System.out.println("1 - Saque Conta Corrente");
			System.out.println("2 - Saque Conta Poupança");
			System.out.println("3 - Saque Conta Salário");
			System.out.println("4 - Depósito Conta Corrente");
			System.out.println("5 - Depósito Conta Poupança");
			System.out.println("6 - Depósito Conta Salário");
			System.out.println("0 - Sair");

			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(System.in);

			System.out.print("Qual operação deseja realizar? ");
			option = scanner.nextInt();

			process(option);
		} while (option != 0);
	}

	public static void process(int option) throws Exception {
		ContaCorrente cc = new ContaCorrente(10001, 10, "C6", 5200.00, 1000.00);
		ContaPoupanca cp = new ContaPoupanca(10002, 21, "Santander", 500.00, 18, 0.5);
		ContaSalario cs = new ContaSalario(10003, 15, "Nubank", 3000.00, 2);
		try {
			switch (option) {
			case 1:
				Scanner entrada = new Scanner(System.in);
				System.out.println("\nSeu saldo é de: R$ " + cc.saldo + ". O limite especial é de: R$ "
						+ cc.getChequeEspecial() + ". Disponível: R$ " + cc.getSaldo());

				System.out.print("Qual o valor do saque? ");
				double quantiaCC = entrada.nextDouble();

				cc.Sacar(quantiaCC);
				break;
			case 2:
				Scanner entrada2 = new Scanner(System.in);
				System.out.print("Qual o dia? (Digite o dia para cálculo dos rendimentos)");
				int dia = entrada2.nextInt();
				cp.taxaDeJurosConta(dia);
				System.out.println("\nSeu saldo é de: R$ " + cp.saldo);

				System.out.print("Qual o valor do saque? ");
				double quantiaCP = entrada2.nextDouble();

				cp.Sacar(quantiaCP);
				break;
			case 3:
				Scanner entrada3 = new Scanner(System.in);
				System.out.println("\nSeu saldo é de: R$ " + cs.saldo);

				System.out.print("Qual o valor do saque? ");
				double quantiaCS = entrada3.nextDouble();

				cs.Sacar(quantiaCS);
				break;
			case 4:
				Scanner entrada4 = new Scanner(System.in);
				System.out.println("\nSeu saldo é de: R$ " + cc.saldo + ". O limite especial é de: R$ "
						+ cc.getChequeEspecial() + ". Disponível: R$ " + cc.getSaldo());

				System.out.print("Qual o valor do depósito? ");
				double quantiaCC2 = entrada4.nextDouble();

				cc.Depositar(quantiaCC2);
				break;
			case 5:
				Scanner entrada5 = new Scanner(System.in);
				System.out.println("\nSeu saldo é de: R$ " + cp.saldo);

				System.out.print("Qual o valor do depósito? ");
				double quantiaCP2 = entrada5.nextDouble();

				cp.Depositar(quantiaCP2);
				break;
			case 6:
				Scanner entrada6 = new Scanner(System.in);
				System.out.println("\nSeu saldo é de: R$ " + cs.saldo);

				System.out.print("Qual o valor do depósito? ");
				double quantiaCS2 = entrada6.nextDouble();

				cs.Depositar(quantiaCS2);
				break;
			}
		} catch (Exception e) {
			System.out.println("O valor digitado é inválido. Revise o valor digitado e tente novamente.");
		}
	}

}
