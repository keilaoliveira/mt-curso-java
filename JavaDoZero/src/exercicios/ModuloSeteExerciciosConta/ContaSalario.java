package exercicios.ModuloSeteExerciciosConta;

public class ContaSalario extends Conta{
	
	public ContaSalario(int numero, int agencia, String banco, double saldo, int limiteSaques) {
		super(numero, agencia, banco, saldo);
		this.limiteSaques = limiteSaques;
	}

	private int limiteSaques;

	public double getSaldo() {
		return saldo;
	}
	
	@Override
	public double Sacar(double quantia) {
		if (limiteSaques > 0) {
			setSaldo(getSaldo() - quantia);
			limiteSaques--;
			System.out.println("Saldo atualizado: R$ " + getSaldo() + ". Você ainda pode sacar " +  limiteSaques + " vez hoje!");
			return quantia;
		}else {
			System.out.println("Você atingiu seu limite de saques diário. Novo saque disponível no próximo dia útil. \n");
		}
		return 0.0;
	}
	
	@Override
	public double Depositar(double quantia) {
		setSaldo(getSaldo() + quantia);
		System.out.println("Saldo atualizado: R$ " + getSaldo());
		return quantia;
	}

	public int getLimiteSaques() {
		return limiteSaques;
	}

	public void setLimiteSaques(int limiteSaques) {
		this.limiteSaques = limiteSaques;
	}

}
