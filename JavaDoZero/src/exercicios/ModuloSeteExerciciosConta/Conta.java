package exercicios.ModuloSeteExerciciosConta;

public abstract class Conta {
	private int numero;
	private int agencia;
	private String banco;
	protected double saldo;
	protected double quantia;

	public double getQuantia() {
		return quantia;
	}

	public void setQuantia(double quantia) {
		this.quantia = quantia;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public abstract double getSaldo();

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public abstract double Sacar(double quantia);
	public abstract double Depositar(double quantia);	
	
	public Conta(int numero, int agencia, String banco, double saldo) {
		this.numero = numero;
		this.agencia = agencia;
		this.banco = banco;
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return "Conta [numero=" + numero + ", agencia=" + agencia + ", banco=" + banco + ", saldo=" + saldo + "]";
	}

}
