package exercicios.cadastroProdutos;

import java.util.Scanner;

import exercicios.cadastroProdutos.models.Produto;

public class Principal {
	
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Digite a descrição do produto: ");
		String descricao = scanner.nextLine();
		
		System.out.print("Digite o id do produto: ");
		int id = scanner.nextInt();
		
		Produto produto1 = new Produto();
		produto1.setId(id);
		produto1.setDescricao(descricao);
		
		System.out.println("Produto cadastrado com sucesso");
		System.out.println("Id: " + id);
		System.out.println("Descrição: " + descricao);
		
		scanner.close();
	}

}
